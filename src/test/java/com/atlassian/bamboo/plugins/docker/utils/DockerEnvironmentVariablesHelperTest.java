package com.atlassian.bamboo.plugins.docker.utils;


import com.atlassian.bamboo.process.EnvironmentVariableAccessorImpl;
import com.atlassian.bamboo.utils.Pair;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.atlassian.bamboo.variable.CustomVariableContext;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class DockerEnvironmentVariablesHelperTest
{
    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private CapabilityContext capabilityContext;
    @Mock
    private CustomVariableContext customVariableContext;

    @InjectMocks
    private EnvironmentVariableAccessorImpl environmentVariableAccessor;

    @Test
    @Parameters(method = "parametersSimpleInput,parametersInputWithApostrophes,parametersInputWithBackslashes,parametersOtherInput")
    public void testSplitEnvironmentVariables(String inputString, List<String> expectedEnvVars)
    {
        final Pair<Map<String, String>, List<String>> envVar = DockerEnvironmentVariablesHelper.splitEnvironmentVariables(environmentVariableAccessor, inputString);

        assertThat(envVar.second, containsInAnyOrder(expectedEnvVars.toArray()));
        assertThat(envVar.first, not(hasKey(isIn(envVar.second))));
    }

    @SuppressWarnings("UnusedDeclaration")
    private Object[] parametersSimpleInput()
    {
        return new Object[]
        {
                //null
                new Object[]{null, Collections.emptyList()},
                //empty
                new Object[]{"", Collections.emptyList()},
                //1 environment variable
                new Object[]{"ENV", Collections.singletonList("ENV")},
                new Object[]{"VAR=val ENV", Collections.singletonList("ENV")},
                new Object[]{"ENV VAR=val", Collections.singletonList("ENV")},
                new Object[]{"VAR1=val1 ENV VAR2=val2", Collections.singletonList("ENV")},
                //2 environment valiables
                new Object[]{"ENV1 ENV2", Arrays.asList("ENV1", "ENV2")},
                new Object[]{"VAR=val ENV1 ENV2", Arrays.asList("ENV1", "ENV2")},
                new Object[]{"ENV1 VAR=val ENV2", Arrays.asList("ENV1", "ENV2")},
                new Object[]{"ENV1 ENV2 VAR=val", Arrays.asList("ENV1", "ENV2")},
                new Object[]{"VAR1=val1 ENV1 VAR2=val2 ENV2 VAR3=val3", Arrays.asList("ENV1", "ENV2")}
        };
    }

    @SuppressWarnings("UnusedDeclaration")
    private Object[] parametersInputWithApostrophes()
    {
        return new Object[]
        {
                //single quote
                new Object[]{"ENV1 VAR=some'v a'l ENV2", Arrays.asList("ENV1", "ENV2")},
                new Object[]{"ENV1 VAR='some'val ENV2", Arrays.asList("ENV1", "ENV2")},
                new Object[]{"ENV1 VAR=some'val' ENV2", Arrays.asList("ENV1", "ENV2")},
                new Object[]{"ENV1 VAR='val1 val2 val3' ENV2", Arrays.asList("ENV1", "ENV2")},
                //double quote
                new Object[]{"ENV1 VAR=some\"v a\"l ENV2", Arrays.asList("ENV1", "ENV2")},
                new Object[]{"ENV1 VAR=\"some\"val ENV2", Arrays.asList("ENV1", "ENV2")},
                new Object[]{"ENV1 VAR=some\"val\" ENV2", Arrays.asList("ENV1", "ENV2")},
                new Object[]{"ENV1 VAR=\"val1 val2 val3\" ENV2", Arrays.asList("ENV1", "ENV2")}
        };
    }

    @SuppressWarnings("UnusedDeclaration")
    private Object[] parametersInputWithBackslashes()
    {
        return new Object[]
        {
                new Object[]{"VAR1=\"c:\\Program Files\\Some Program\\some executable.exe\" ENV1" +
                                " VAR2=\"c:\\My Documents\\My Trip\\some file.png\"", Collections.singletonList("ENV1")},
                new Object[]{"VAR1=val1\\val2\\val3 ENV1", Collections.singletonList("ENV1")},
                new Object[]{"ENV1 VAR1=\"val1\\val2 val3\\val4\"", Collections.singletonList("ENV1")}
        };
    }

    @SuppressWarnings("UnusedDeclaration")
    private Object[] parametersOtherInput()
    {
        return new Object[]
        {
                //empty variable and variable to pass
                new Object[]{"VAR=", Collections.emptyList()},
                new Object[]{"VAR= ENV1", Collections.singletonList("ENV1")},
                new Object[]{"ENV1 VAR=", Collections.singletonList("ENV1")},
                //overriding variables
                new Object[]{"VAR=val VAR", Collections.singletonList("VAR")},
                new Object[]{"VAR VAR=val", Collections.emptyList()},
                //names with apostrophes and slashes (Windows)
                new Object[]{"EN\"V", Collections.singletonList("EN\"V")},
                new Object[]{"EN\'V", Collections.singletonList("EN\'V")},
                new Object[]{"EN\\V", Collections.singletonList("EN\\V")}
        };
    }
}
