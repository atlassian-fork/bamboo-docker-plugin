package com.atlassian.bamboo.plugins.docker.export;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.task.AnyTask;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.DockerBuildImageTask;
import com.atlassian.bamboo.specs.model.task.docker.AbstractDockerTaskProperties;
import com.atlassian.bamboo.specs.model.task.docker.DockerBuildImageTaskProperties;
import com.atlassian.bamboo.task.TaskContainer;
import com.atlassian.bamboo.task.TaskDefinitionImpl;
import com.atlassian.bamboo.task.export.TaskDefinitionExporter;
import com.atlassian.bamboo.task.export.TaskValidationContext;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKERFILE;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKERFILE_OPTION;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKERFILE_OPTION_EXISTING;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKERFILE_OPTION_INLINE;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_BUILD;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_PULL;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.FILENAME;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.NOCACHE;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.REPOSITORY;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.SAVE;
import static com.atlassian.bamboo.task.TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES;
import static com.atlassian.bamboo.task.TaskConfigConstants.CFG_WORKING_SUBDIRECTORY;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.isEmptyString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public class BuildImageTaskExporterTest {
    private static final String IMAGE_FILE_NAME = "image file name";
    private static final String DOCKER_HUB_ORG = "docker-hub.org";
    private static final String SUBDIRECTORY = "/tmp/working-dir";
    private static final String ENV_VARIABLES = "-DJAVA_HOME=/opt/jdk-8";
    private static final String DOCKERFILE_CONTENT = "FROM ubuntu";

    private TaskDefinitionExporter buildImageTaskExporter;

    @Before
    public void setUp() {
        buildImageTaskExporter = new BuildImageTaskExporter();
    }

    @Test
    public void testToTaskConfigFullConfiguration() {
        DockerBuildImageTask buildImageTask = new DockerBuildImageTask()
                .imageFilename(IMAGE_FILE_NAME)
                .dockerfile(DOCKERFILE_CONTENT)
                .saveAsFile(true)
                .useCache(false)
                .imageName(DOCKER_HUB_ORG)
                .workingSubdirectory(SUBDIRECTORY)
                .environmentVariables(ENV_VARIABLES);

        Map<String, String> result = buildImageTaskExporter
                .toTaskConfiguration(mock(TaskContainer.class), EntityPropertiesBuilders.build(buildImageTask));


        assertThat(result, allOf(hasEntry(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_BUILD),
                hasEntry(DOCKERFILE_OPTION, DOCKERFILE_OPTION_INLINE),
                hasEntry(DOCKERFILE, DOCKERFILE_CONTENT),
                hasEntry(REPOSITORY, DOCKER_HUB_ORG),
                hasEntry(NOCACHE, "true"),
                hasEntry(SAVE, "true"),
                hasEntry(FILENAME, IMAGE_FILE_NAME),
                hasEntry(CFG_ENVIRONMENT_VARIABLES, ENV_VARIABLES),
                hasEntry(CFG_WORKING_SUBDIRECTORY, SUBDIRECTORY)));
    }

    @Test
    public void testToTaskConfigMinimalConfiguration() {
        DockerBuildImageTask buildImageTask = new DockerBuildImageTask()
                .imageFilename(IMAGE_FILE_NAME)
                .imageName(DOCKER_HUB_ORG)
                .dockerfileInWorkingDir();

        Map<String, String> result = buildImageTaskExporter
                .toTaskConfiguration(mock(TaskContainer.class), EntityPropertiesBuilders.build(buildImageTask));

        assertThat(result, allOf(hasEntry(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_BUILD),
                hasEntry(DOCKERFILE_OPTION, DOCKERFILE_OPTION_EXISTING),
                hasEntry(REPOSITORY, DOCKER_HUB_ORG),
                hasEntry(NOCACHE, "false"),
                hasEntry(SAVE, "false"),
                hasEntry(FILENAME, IMAGE_FILE_NAME),
                hasEntry(CFG_ENVIRONMENT_VARIABLES, ""),
                hasEntry(CFG_WORKING_SUBDIRECTORY, "")));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testToTaskConfigWrongProperties() {
        AnyTask task = new AnyTask(new AtlassianModule("module:key"));

        buildImageTaskExporter
                .toTaskConfiguration(mock(TaskContainer.class), EntityPropertiesBuilders.build(task));
    }

    @Test
    public void toSpecsEntityFullConfiguration() {
        ImmutableMap.Builder<String, String> configuration = ImmutableMap.builder();
        configuration.put(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_BUILD);
        configuration.put(DOCKERFILE_OPTION, DOCKERFILE_OPTION_INLINE);
        configuration.put(DOCKERFILE, DOCKERFILE_CONTENT);
        configuration.put(REPOSITORY, DOCKER_HUB_ORG);
        configuration.put(NOCACHE, "true");
        configuration.put(SAVE, "true");
        configuration.put(FILENAME, IMAGE_FILE_NAME);
        configuration.put(CFG_ENVIRONMENT_VARIABLES, ENV_VARIABLES);
        configuration.put(CFG_WORKING_SUBDIRECTORY, ENV_VARIABLES);


        DockerBuildImageTaskProperties result = EntityPropertiesBuilders.build(buildImageTaskExporter.toSpecsEntity(new TaskDefinitionImpl(0,
                AbstractDockerTaskProperties.MODULE_KEY.getCompleteModuleKey(),
                "",
                configuration.build())));


        assertThat(result.getDockerfileContent(), is(DockerBuildImageTaskProperties.DockerfileContent.INLINE));
        assertThat(result.getImageName(), equalTo(DOCKER_HUB_ORG));
        assertThat(result.getDockerfile(), equalTo(DOCKERFILE_CONTENT));
        assertThat(result.isUseCache(), is(false));
        assertThat(result.isSaveAsFile(), is(true));
        assertThat(result.getImageFilename(), equalTo(IMAGE_FILE_NAME));
        assertThat(result.getEnvironmentVariables(), equalTo(ENV_VARIABLES));
        assertThat(result.getWorkingSubdirectory(), equalTo(ENV_VARIABLES));
    }

    @Test
    public void toSpecsEntityMinimalConfiguration() {
        DockerBuildImageTaskProperties expectedProperties = EntityPropertiesBuilders
                .build(new DockerBuildImageTask()
                        .dockerfileInWorkingDir()
                        .imageName(DOCKER_HUB_ORG)
                );

        ImmutableMap.Builder<String, String> configuration = ImmutableMap.builder();
        configuration.put(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_BUILD);
        configuration.put(DOCKERFILE_OPTION, DOCKERFILE_OPTION_EXISTING);
        configuration.put(FILENAME, "");
        configuration.put(REPOSITORY, DOCKER_HUB_ORG);
        configuration.put(NOCACHE, "false");
        configuration.put(SAVE, "false");
        configuration.put(CFG_ENVIRONMENT_VARIABLES, "");
        configuration.put(CFG_WORKING_SUBDIRECTORY, "");


        DockerBuildImageTaskProperties result = EntityPropertiesBuilders.build(buildImageTaskExporter.toSpecsEntity(new TaskDefinitionImpl(0,
                AbstractDockerTaskProperties.MODULE_KEY.getCompleteModuleKey(),
                "",
                configuration.build())));

        assertThat(result.getDockerfileContent(), is(DockerBuildImageTaskProperties.DockerfileContent.WORKING_DIR));
        assertThat(result.getImageName(), equalTo(DOCKER_HUB_ORG));
        assertThat(result.getDockerfile(), isEmptyOrNullString());
        assertThat(result.isUseCache(), is(true));
        assertThat(result.isSaveAsFile(), is(false));
        assertThat(result.getImageFilename(), isEmptyString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void toSpecsEntityWrongTaskDefinition() {
        buildImageTaskExporter
                .toSpecsEntity(new TaskDefinitionImpl(0L, "module:key", "", Collections.emptyMap()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void toSpecsEntityWrongCommandOption() {
        buildImageTaskExporter
                .toSpecsEntity(new TaskDefinitionImpl(0L,
                        AbstractDockerTaskProperties.MODULE_KEY.getCompleteModuleKey(),
                        "",
                        ImmutableMap.of(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_PULL)));
    }

    @Test
    public void validate() {
        List<ValidationProblem> result = buildImageTaskExporter
                .validate(mock(TaskValidationContext.class), mock(TaskProperties.class));

        assertTrue(result.isEmpty());
    }
}