package com.atlassian.bamboo.plugins.docker.export;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.task.AnyTask;
import com.atlassian.bamboo.specs.api.model.plan.JobProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.DockerRunContainerTask;
import com.atlassian.bamboo.specs.model.task.docker.AbstractDockerTaskProperties;
import com.atlassian.bamboo.specs.model.task.docker.DockerRunContainerTaskProperties;
import com.atlassian.bamboo.task.TaskContainer;
import com.atlassian.bamboo.task.TaskDefinitionImpl;
import com.atlassian.bamboo.task.export.TaskDefinitionExporter;
import com.atlassian.bamboo.task.export.TaskValidationContext;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.ADDITIONAL_ARGS;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.COMMAND;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.CONTAINER_DATA_VOLUME_PREFIX;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.CONTAINER_PORT_PREFIX;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DEFAULT_CONTAINER_WORK_DIR;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DEFAULT_TIMEOUT_SECONDS;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DETACH;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_PULL;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_RUN;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.ENV_VARS;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.HOST_DIRECTORY_PREFIX;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.HOST_PORT_PREFIX;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.IMAGE;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.LINK;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.NAME;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.SERVICE_TIMEOUT;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.SERVICE_URL_PATTERN;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.SERVICE_WAIT;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.TASK_WORK_DIR_PLACEHOLDER;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.WORK_DIR;
import static com.atlassian.bamboo.specs.model.task.docker.DockerRunContainerTaskProperties.VALIDATION_CONTEXT;
import static com.atlassian.bamboo.task.TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES;
import static com.atlassian.bamboo.task.TaskConfigConstants.CFG_WORKING_SUBDIRECTORY;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RunContainerTaskExporterTest {

    private static final String DOCKER_IMAGE = "bamboo:5.11";
    private static final String SUBDIRECTORY = "/tmp/working-dir";
    private static final String ENV_VARIABLES = "-DJAVA_HOME=/opt/jdk-8";
    private static final String CONTAINER_NAME = "myContainer";
    private static final String CONTAINER_ENV_VARIABLES = "-DCONTAINER_JAVA_HOME=/opt/jdk-5";
    private static final String CONTAINER_COMMAND = "/bin/bash";
    private static final String CONTAINER_WORKING_DIRECTORY = "/tmp/";
    private static final String ADDITIONAL_ARGUMENTS = "--memory=\"64m\"";
    private static final String CONTAINER_SERVICE_URL_PATTERN = "https://localhost:${docker.port}";
    private static final int TIMEOUT = 150;

    //directories mappings
    private static final String HOME = "/home";
    private static final String HOME2 = "/home2";
    private static final String ETC = "/etc";
    private static final String ETC2 = "/etc2";

    private TaskDefinitionExporter runContainerTaskExporter;

    @Before
    public void setUp() throws Exception {
        runContainerTaskExporter = new RunContainerTaskExporter();
    }

    @Test
    public void testToTaskConfigFullConfiguration() throws Exception {
        DockerRunContainerTask runContainerTask = new DockerRunContainerTask()
                .imageName(DOCKER_IMAGE)
                .detachContainer(true)
                .containerName(CONTAINER_NAME)
                .appendPortMapping(18080, 8080)
                .appendPortMapping(15432, 5432)
                .waitToStart(true)
                .serviceURLPattern(CONTAINER_SERVICE_URL_PATTERN)
                .serviceTimeoutInSeconds(TIMEOUT)
                .linkToDetachedContainers(true)
                .containerEnvironmentVariables(CONTAINER_ENV_VARIABLES)
                .containerCommand(CONTAINER_COMMAND)
                .containerWorkingDirectory(CONTAINER_WORKING_DIRECTORY)
                .additionalArguments(ADDITIONAL_ARGUMENTS)
                .appendVolumeMapping(HOME, HOME2)
                .appendVolumeMapping(ETC, ETC2)
                .workingSubdirectory(SUBDIRECTORY)
                .environmentVariables(ENV_VARIABLES);

        Map<String, String> result = runContainerTaskExporter
                .toTaskConfiguration(mock(TaskContainer.class), EntityPropertiesBuilders.build(runContainerTask));
        Map<Integer, Integer> resultPortMappings = configMapToPortMappings(result);
        Map<String, String> resultVolumeMappings = configMapToVolumeMappings(result);

        assertThat(result, allOf(hasEntry(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_RUN),
                hasEntry(IMAGE, DOCKER_IMAGE),
                hasEntry(COMMAND, CONTAINER_COMMAND),
                hasEntry(NAME, CONTAINER_NAME),
                hasEntry(DETACH, "true"),
                hasEntry(LINK, "true"),
                hasEntry(ENV_VARS, CONTAINER_ENV_VARIABLES),
                hasEntry(WORK_DIR, CONTAINER_WORKING_DIRECTORY),
                hasEntry(ADDITIONAL_ARGS, ADDITIONAL_ARGUMENTS),
                hasEntry(SERVICE_WAIT, "true"),
                hasEntry(SERVICE_URL_PATTERN, CONTAINER_SERVICE_URL_PATTERN),
                hasEntry(SERVICE_TIMEOUT, String.valueOf(TIMEOUT)),
                hasEntry(CFG_ENVIRONMENT_VARIABLES, ENV_VARIABLES),
                hasEntry(CFG_WORKING_SUBDIRECTORY, SUBDIRECTORY)));

        assertThat(resultPortMappings, allOf(
                hasEntry(18080, 8080),
                hasEntry(15432, 5432)
        ));
        assertThat(resultVolumeMappings, allOf(
                hasEntry(HOME, HOME2),
                hasEntry(ETC, ETC2)
        ));
    }

    @Test
    public void testToTaskConfigMinimalConfiguration() throws Exception {
        DockerRunContainerTask runContainerTask = new DockerRunContainerTask()
                .imageName(DOCKER_IMAGE);

        Map<String, String> result = runContainerTaskExporter
                .toTaskConfiguration(mock(TaskContainer.class), EntityPropertiesBuilders.build(runContainerTask));

        assertThat(result, allOf(hasEntry(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_RUN),
                hasEntry(IMAGE, DOCKER_IMAGE),
                hasEntry(COMMAND, ""),
                hasEntry(NAME, ""),
                hasEntry(DETACH, "false"),
                hasEntry(LINK, "false"),
                hasEntry(ENV_VARS, ""),
                hasEntry(WORK_DIR, DEFAULT_CONTAINER_WORK_DIR),
                hasEntry(ADDITIONAL_ARGS, ""),
                hasEntry(SERVICE_WAIT, "false"),
                hasEntry(SERVICE_URL_PATTERN, ""),
                hasEntry(SERVICE_TIMEOUT, String.valueOf(DEFAULT_TIMEOUT_SECONDS)),
                hasEntry(HOST_DIRECTORY_PREFIX + 0, TASK_WORK_DIR_PLACEHOLDER),
                hasEntry(CONTAINER_DATA_VOLUME_PREFIX + 0, DEFAULT_CONTAINER_WORK_DIR),
                hasEntry(CFG_ENVIRONMENT_VARIABLES, ""),
                hasEntry(CFG_WORKING_SUBDIRECTORY, "")));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testToTaskConfigWrongProperties() throws Exception {
        AnyTask task = new AnyTask(new AtlassianModule("module:key"));

        runContainerTaskExporter
                .toTaskConfiguration(mock(TaskContainer.class), EntityPropertiesBuilders.build(task));
    }

    @Test
    public void toSpecsEntityFullConfiguration() throws Exception {
        DockerRunContainerTaskProperties expectedProperties = EntityPropertiesBuilders
                .build(new DockerRunContainerTask()
                        .imageName(DOCKER_IMAGE)
                        .detachContainer(true)
                        .containerName(CONTAINER_NAME)
                        .appendPortMapping(18080, 8080)
                        .appendPortMapping(15432, 5432)
                        .waitToStart(true)
                        .serviceURLPattern(CONTAINER_SERVICE_URL_PATTERN)
                        .serviceTimeoutInSeconds(TIMEOUT)
                        .linkToDetachedContainers(true)
                        .containerEnvironmentVariables(CONTAINER_ENV_VARIABLES)
                        .containerCommand(CONTAINER_COMMAND)
                        .containerWorkingDirectory(CONTAINER_WORKING_DIRECTORY)
                        .additionalArguments(ADDITIONAL_ARGUMENTS)
                        .appendVolumeMapping(HOME, HOME2)
                        .appendVolumeMapping(ETC, ETC2)
                        .workingSubdirectory(SUBDIRECTORY)
                        .environmentVariables(ENV_VARIABLES)
                );

        ImmutableMap.Builder<String, String> configuration = ImmutableMap.builder();
        configuration.put(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_RUN);
        configuration.put(IMAGE, DOCKER_IMAGE);
        configuration.put(COMMAND, CONTAINER_COMMAND);
        configuration.put(NAME, CONTAINER_NAME);
        configuration.put(DETACH, "true");
        configuration.put(LINK, "true");
        configuration.put(ENV_VARS, CONTAINER_ENV_VARIABLES);
        configuration.put(WORK_DIR, CONTAINER_WORKING_DIRECTORY);
        configuration.put(ADDITIONAL_ARGS, ADDITIONAL_ARGUMENTS);
        configuration.put(SERVICE_WAIT, "true");
        configuration.put(SERVICE_URL_PATTERN, CONTAINER_SERVICE_URL_PATTERN);
        configuration.put(SERVICE_TIMEOUT, String.valueOf(TIMEOUT));
        configuration.put(CFG_ENVIRONMENT_VARIABLES, ENV_VARIABLES);
        configuration.put(CFG_WORKING_SUBDIRECTORY, SUBDIRECTORY);

        configuration.put(HOST_PORT_PREFIX + "0", "18080");
        configuration.put(CONTAINER_PORT_PREFIX + "0", "8080");
        configuration.put(HOST_PORT_PREFIX + "1", "15432");
        configuration.put(CONTAINER_PORT_PREFIX + "1", "5432");

        configuration.put(HOST_DIRECTORY_PREFIX + "0", HOME);
        configuration.put(CONTAINER_DATA_VOLUME_PREFIX + "0", HOME2);
        configuration.put(HOST_DIRECTORY_PREFIX + "1", ETC);
        configuration.put(CONTAINER_DATA_VOLUME_PREFIX + "1", ETC2);
        configuration.put(HOST_DIRECTORY_PREFIX + "2", TASK_WORK_DIR_PLACEHOLDER);
        configuration.put(CONTAINER_DATA_VOLUME_PREFIX + "2", DEFAULT_CONTAINER_WORK_DIR);

        DockerRunContainerTaskProperties result = EntityPropertiesBuilders.build((DockerRunContainerTask) runContainerTaskExporter.toSpecsEntity(new TaskDefinitionImpl(0,
                AbstractDockerTaskProperties.MODULE_KEY.getCompleteModuleKey(),
                "",
                configuration.build())));


        assertThat(result, equalTo(expectedProperties));
    }

    @Test
    public void toSpecsEntityMinimalConfiguration() throws Exception {
        DockerRunContainerTaskProperties expectedProperties = EntityPropertiesBuilders
                .build(new DockerRunContainerTask()
                        .imageName(DOCKER_IMAGE)
                );

        ImmutableMap.Builder<String, String> configuration = ImmutableMap.builder();
        configuration.put(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_RUN);
        configuration.put(IMAGE, DOCKER_IMAGE);
        configuration.put(COMMAND, "");
        configuration.put(NAME, "");
        configuration.put(DETACH, "false");
        configuration.put(LINK, "false");
        configuration.put(ENV_VARS, "");
        configuration.put(WORK_DIR, DEFAULT_CONTAINER_WORK_DIR);
        configuration.put(ADDITIONAL_ARGS, "");
        configuration.put(SERVICE_WAIT, "false");
        configuration.put(SERVICE_URL_PATTERN, "");
        configuration.put(SERVICE_TIMEOUT, String.valueOf(DEFAULT_TIMEOUT_SECONDS));
        configuration.put(CFG_ENVIRONMENT_VARIABLES, "");
        configuration.put(CFG_WORKING_SUBDIRECTORY, "");
        configuration.put(ADDITIONAL_ARGUMENTS, "");
        configuration.put(HOST_DIRECTORY_PREFIX + "0", TASK_WORK_DIR_PLACEHOLDER);
        configuration.put(CONTAINER_DATA_VOLUME_PREFIX + "0", DEFAULT_CONTAINER_WORK_DIR);

        DockerRunContainerTaskProperties result = EntityPropertiesBuilders.build((DockerRunContainerTask) runContainerTaskExporter.toSpecsEntity(new TaskDefinitionImpl(0,
                AbstractDockerTaskProperties.MODULE_KEY.getCompleteModuleKey(),
                "",
                configuration.build())));

        assertThat(result.getImageName(), equalTo(DOCKER_IMAGE));
        assertThat(result.getContainerCommand(), isEmptyString());
        assertThat(result.getContainerName(), isEmptyString());
        assertThat(result.isDetachedContainer(), is(false));
        assertThat(result.isLinkToDetachedContainers(), is(false));
        assertThat(result.getContainerEnvironmentVariables(), isEmptyString());
        assertThat(result.getContainerWorkingDirectory(), equalTo(DEFAULT_CONTAINER_WORK_DIR));
        assertThat(result.getAdditionalArguments(), isEmptyString());
        assertThat(result.isWaitToStart(), is(false));
        assertThat(result.getServiceURLPattern(), isEmptyString());
        assertThat(result.getServiceTimeout(), equalTo(120L));
        assertThat(result.getWorkingSubdirectory(), isEmptyString());
        assertThat(result.getEnvironmentVariables(), isEmptyString());
        assertThat(result.getVolumeMappings(),
                hasEntry(TASK_WORK_DIR_PLACEHOLDER, DEFAULT_CONTAINER_WORK_DIR));
        assertThat(result.getVolumeMappings().size(), equalTo(1));
        assertTrue(result.getPortMappings().isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void toSpecsEntityWrongTaskDefinition() throws Exception {
        runContainerTaskExporter
                .toSpecsEntity(new TaskDefinitionImpl(0L, "module:key", "", Collections.emptyMap()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void toSpecsEntityWrongCommandOption() throws Exception {
        runContainerTaskExporter
                .toSpecsEntity(new TaskDefinitionImpl(0L,
                        AbstractDockerTaskProperties.MODULE_KEY.getCompleteModuleKey(),
                        "",
                        ImmutableMap.of(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_PULL)));
    }

    @Test
    public void testValidateFirstTaskInJob() throws Exception {
        TaskValidationContext context = mock(TaskValidationContext.class);
        JobProperties job = EntityPropertiesBuilders.build(new Job("Name", "KY"));
        when(context.getOwnerJob()).thenReturn(Optional.of(job));

        List<ValidationProblem> result = runContainerTaskExporter
                .validate(context, mock(TaskProperties.class));

        assertTrue(result.isEmpty());
    }

    @Test
    public void testValidateNoOwnerJob() throws Exception {
        TaskValidationContext context = mock(TaskValidationContext.class);
        when(context.getOwnerJob()).thenReturn(Optional.empty());

        List<ValidationProblem> result = runContainerTaskExporter
                .validate(context, mock(TaskProperties.class));

        assertTrue(result.isEmpty());
    }

    @Test
    public void testValidateNoDuplicatedNames() throws Exception {
        TaskValidationContext context = mock(TaskValidationContext.class);

        DockerRunContainerTask runTask = new DockerRunContainerTask()
                .imageName("image2")
                .detachContainer(true)
                .containerName("containerName2");

        JobProperties job = EntityPropertiesBuilders.build(new Job("Name", "KY").tasks(
                new DockerRunContainerTask()
                        .imageName("image1")
                        .detachContainer(true)
                        .containerName("containerName1"),
                runTask
        ));

        when(context.getOwnerJob()).thenReturn(Optional.of(job));

        List<ValidationProblem> result = runContainerTaskExporter
                .validate(context, mock(TaskProperties.class));

        assertTrue(result.isEmpty());
    }

    @Test
    public void testValidateDuplicatedName() throws Exception {
        TaskValidationContext context = mock(TaskValidationContext.class);
        String containerName = "containerName";

        DockerRunContainerTask runTask = new DockerRunContainerTask()
                .imageName("image2")
                .detachContainer(true)
                .containerName(containerName);

        JobProperties job = EntityPropertiesBuilders.build(new Job("Name", "KY").tasks(
                new DockerRunContainerTask()
                        .imageName("image1")
                        .detachContainer(true)
                        .containerName(containerName),
                runTask
        ));

        when(context.getOwnerJob()).thenReturn(Optional.of(job));

        List<ValidationProblem> result = runContainerTaskExporter
                .validate(context, EntityPropertiesBuilders.build(runTask));

        assertThat(result, containsInAnyOrder(new ValidationProblem(VALIDATION_CONTEXT, String.format("Name '%s' is already assigned to a detached container in this job", containerName))));
    }

    private Map<Integer, Integer> configMapToPortMappings(Map<String, String> result) {
        ImmutableMap.Builder<Integer, Integer> mappings = ImmutableMap.builder();
        for (Map.Entry<String, String> entry : result.entrySet()) {
            if (entry.getKey().contains(HOST_PORT_PREFIX)) {
                Integer index = Integer.parseInt(result.get(CONTAINER_PORT_PREFIX +
                        entry.getKey().substring(HOST_PORT_PREFIX.length())));
                mappings.put(Integer.parseInt(entry.getValue()), index);
            }
        }
        return mappings.build();
    }

    private Map<String, String> configMapToVolumeMappings(Map<String, String> result) {
        ImmutableMap.Builder<String, String> mappings = ImmutableMap.builder();
        for (Map.Entry<String, String> entry : result.entrySet()) {
            if (entry.getKey().contains(HOST_DIRECTORY_PREFIX)) {
                String index = result.get(CONTAINER_DATA_VOLUME_PREFIX +
                        entry.getKey().substring(HOST_DIRECTORY_PREFIX.length()));
                mappings.put(entry.getValue(), index);
            }
        }
        return mappings.build();
    }
}