package com.atlassian.bamboo.plugins.docker.tasks.cli;

import com.atlassian.bamboo.credentials.CredentialsAccessor;
import com.atlassian.bamboo.crypto.instance.SecretEncryptionService;
import com.atlassian.bamboo.mock.MockSecretEncryptionService;
import com.atlassian.bamboo.plugins.docker.validation.ConfigValidatorFactory;
import com.atlassian.bamboo.task.TaskConfiguratorHelperImpl;
import com.atlassian.bamboo.task.TaskContextHelperService;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.webwork.util.ActionParametersMapImpl;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.collect.Maps;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.stubbing.Answer;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.CREDENTIALS_DOCKER_CFG;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.CREDENTIALS_SOURCE_SHARED_CREDENTIALS;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.CREDENTIALS_SOURCE_USER;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_PULL;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_PUSH;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.PASSWORD;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.PULL_CREDENTIALS_SOURCE;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.PULL_PASSWORD;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.PULL_SHARED_CREDENTIALS_ID;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.PULL_USERNAME;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.PUSH_CREDENTIALS_SOURCE;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.PUSH_SHARED_CREDENTIALS_ID;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.USERNAME;
import static com.google.common.collect.ImmutableMap.of;
import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@RunWith(JUnitParamsRunner.class)
public class DockerCliTaskConfiguratorTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private TaskContextHelperService taskContextHelperService;
    @Mock
    private I18nResolver i18nResolver;
    private SecretEncryptionService secretEncryptionService = new MockSecretEncryptionService();

    @InjectMocks
    private TaskConfiguratorHelperImpl taskConfiguratorHelper;
    private DockerCliTaskConfigurator configurator;


    @Before
    public void setUp()
    {
        when(i18nResolver.getText(anyString()))
                .then((Answer<String>) invocation -> invocation.getArgument(0));
        configurator = new DockerCliTaskConfigurator(mock(CredentialsAccessor.class),
                mock(ConfigValidatorFactory.class),
                i18nResolver,
                taskContextHelperService,
                taskConfiguratorHelper,
                secretEncryptionService
        );
    }

    @Parameters(method = "generateTaskConfigCredentials")
    @Test
    public void testGenerateTaskConfigCredentials(Map<String, String> params,
                                                  Map<String, String> expectedKeys,
                                                  List<String> notExpectingKeys) {

        Map<String, String> result = configurator
                .generateTaskConfigMap(new ActionParametersMapImpl(params), null);

        expectedKeys.forEach((key, value) -> {
            assertThat(result, hasEntry(key, value));
        });

        notExpectingKeys.forEach(key ->
                assertThat(result, not(hasKey(key)))
        );
    }

    public Object[][] generateTaskConfigCredentials() {
        return new Object[][]{
                new Object[]{
                        //expected entries
                        of(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_PUSH,
                                PUSH_CREDENTIALS_SOURCE, CREDENTIALS_DOCKER_CFG),
                        //expected entries
                        of(PUSH_CREDENTIALS_SOURCE, CREDENTIALS_DOCKER_CFG),
                        //not expected keys
                        newArrayList(USERNAME, PASSWORD, PUSH_SHARED_CREDENTIALS_ID)
                },
                new Object[]{
                        //expected entries
                        of(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_PUSH,
                                PUSH_CREDENTIALS_SOURCE, CREDENTIALS_SOURCE_USER,
                                USERNAME, "user", PASSWORD, "password"),
                        //expected entries
                        of(PUSH_CREDENTIALS_SOURCE, CREDENTIALS_SOURCE_USER,
                                USERNAME, "user",
                                PASSWORD, secretEncryptionService.encrypt("password")),
                        //not expected keys
                        newArrayList(PUSH_SHARED_CREDENTIALS_ID)
                },
                new Object[]{
                        //expected entries
                        of(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_PUSH,
                                PUSH_CREDENTIALS_SOURCE, CREDENTIALS_SOURCE_SHARED_CREDENTIALS,
                                PUSH_SHARED_CREDENTIALS_ID, "5"),
                        //expected entries
                        of(PUSH_CREDENTIALS_SOURCE, CREDENTIALS_SOURCE_SHARED_CREDENTIALS,
                                PUSH_SHARED_CREDENTIALS_ID, "5"),
                        //not expected keys
                        newArrayList(USERNAME, PASSWORD)
                },
                new Object[]{
                        //parameters map
                        of(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_PULL,
                                PULL_CREDENTIALS_SOURCE, CREDENTIALS_DOCKER_CFG),
                        //expected entries
                        of(PULL_CREDENTIALS_SOURCE, CREDENTIALS_DOCKER_CFG),
                        //not expected keys
                        newArrayList(PULL_USERNAME, PULL_PASSWORD, PULL_SHARED_CREDENTIALS_ID)
                },
                new Object[]{
                        //parameters map
                        of(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_PULL,
                                PULL_CREDENTIALS_SOURCE, CREDENTIALS_SOURCE_USER,
                                PULL_USERNAME, "user", PULL_PASSWORD, "password"),

                        //expected entries
                        of(PULL_CREDENTIALS_SOURCE, CREDENTIALS_SOURCE_USER,
                                PULL_USERNAME, "user", PULL_PASSWORD, secretEncryptionService.encrypt("password")),
                        //not expected keys
                        newArrayList(PULL_SHARED_CREDENTIALS_ID)
                },
                new Object[]{
                        //parameters map
                        of(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_PULL,
                                PULL_CREDENTIALS_SOURCE, CREDENTIALS_SOURCE_SHARED_CREDENTIALS,
                                PUSH_SHARED_CREDENTIALS_ID, "5"),
                        //expected entries
                        of(PULL_CREDENTIALS_SOURCE, CREDENTIALS_SOURCE_SHARED_CREDENTIALS,
                                PUSH_SHARED_CREDENTIALS_ID, "5"),
                        //not expected keys
                        newArrayList(PULL_USERNAME, PULL_PASSWORD)
                }
        };
    }

    @Parameters(method = "editConfigCredentials")
    @Test
    public void testEditConfigCredentials(Map<String, String> params,
                                          Map<String, String> expectedKeys,
                                          List<String> notExpectingKeys) {

        TaskDefinition taskDefinition = mock(TaskDefinition.class);
        when(taskContextHelperService
                .getTasksBeforeTaskId(anyMap(), anyLong()))
                .thenReturn(Collections.emptyList());
        when(taskDefinition.getConfiguration()).thenReturn(params);

        Map<String, Object> result = Maps.newHashMap();
        configurator.
                populateContextForEdit(result, taskDefinition);

        expectedKeys.forEach((key, value) -> {
            assertThat(result, hasEntry(key, value));
        });

        notExpectingKeys.forEach(key ->
                assertThat(result, not(hasKey(key)))
        );
    }

    private Object[][] editConfigCredentials() {
        return new Object[][]{
                new Object[]{
                        //parameters
                        of(PULL_SHARED_CREDENTIALS_ID, DOCKER_COMMAND_OPTION_PUSH),
                        //expected entries
                        of(PUSH_CREDENTIALS_SOURCE, CREDENTIALS_DOCKER_CFG),
                        //not expected keys
                        newArrayList(USERNAME, PASSWORD, PUSH_SHARED_CREDENTIALS_ID)
                },
                new Object[]{
                        //parameters
                        of(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_PULL),
                        //expected entries
                        of(PUSH_CREDENTIALS_SOURCE, CREDENTIALS_DOCKER_CFG),
                        //not expected keys
                        newArrayList(PULL_USERNAME, PULL_PASSWORD, PULL_SHARED_CREDENTIALS_ID)
                },
                new Object[]{
                        //expected entries
                        of(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_PUSH,
                                PUSH_SHARED_CREDENTIALS_ID, "5"),
                        //expected entries
                        of(PUSH_CREDENTIALS_SOURCE, CREDENTIALS_SOURCE_SHARED_CREDENTIALS,
                                PUSH_SHARED_CREDENTIALS_ID, "5"),
                        //not expected keys
                        newArrayList(USERNAME, PASSWORD)
                },
                new Object[]{
                        //parameters map
                        of(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_PULL,
                                PULL_SHARED_CREDENTIALS_ID, "5"),
                        //expected entries
                        of(PULL_CREDENTIALS_SOURCE, CREDENTIALS_SOURCE_SHARED_CREDENTIALS,
                                PULL_SHARED_CREDENTIALS_ID, "5"),
                        //not expected keys
                        newArrayList(PULL_USERNAME, PULL_PASSWORD)
                },
                new Object[]{
                        //parameters map
                        of(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_PULL,
                                PULL_USERNAME, "user", PULL_PASSWORD, "password"),
                        //expected entries
                        of(PULL_CREDENTIALS_SOURCE, CREDENTIALS_SOURCE_USER,
                                PULL_USERNAME, "user", PULL_PASSWORD, "password"),
                        //not expected keys
                        newArrayList(PULL_SHARED_CREDENTIALS_ID)
                },
                new Object[]{
                        //parameters map
                        of(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_PUSH,
                                USERNAME, "user", PASSWORD, "password"),
                        //expected entries
                        of(PUSH_CREDENTIALS_SOURCE, CREDENTIALS_SOURCE_USER,
                                USERNAME, "user", PASSWORD, "password"),
                        //not expected keys
                        newArrayList(PUSH_SHARED_CREDENTIALS_ID)
                },
        };
    }
}