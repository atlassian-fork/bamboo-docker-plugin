package com.atlassian.bamboo.plugins.docker.export;

import com.atlassian.bamboo.plugins.docker.service.DockerExporterFactory;
import com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator;
import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.task.TaskContainer;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.export.TaskDefinitionExporter;
import com.atlassian.bamboo.task.export.TaskValidationContext;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DockerTaskExporterTest {

    private static final String EXPORTER_KEY = "exporterKey";
    private TaskDefinitionExporter taskExporterSUT;

    private DockerExporterFactory dockerExporterFactory;

    @Before
    public void setUp() throws Exception {
        dockerExporterFactory = mock(DockerExporterFactory.class);

        taskExporterSUT = new DockerTaskExporter(dockerExporterFactory);
    }

    @Test
    public void testToConfigurationOK() throws Exception {
        TaskProperties taskProperties = mock(TaskProperties.class);
        TaskContainer taskContainer = mock(TaskContainer.class);
        TaskDefinitionExporter taskExporterMock = mock(TaskDefinitionExporter.class);
        when(dockerExporterFactory.createExporter(taskProperties)).thenReturn(taskExporterMock);
        when(taskExporterMock.toTaskConfiguration(taskContainer, taskProperties)).thenReturn(Collections.emptyMap());

        Map<String, String> result = taskExporterSUT.toTaskConfiguration(taskContainer, taskProperties);

        verify(dockerExporterFactory, times(1)).createExporter(taskProperties);
        verify(taskExporterMock, times(1)).toTaskConfiguration(taskContainer, taskProperties);
        assertThat(result, equalTo(Collections.emptyMap()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testToTaskConfigurationWhenCantExportProperties() throws Exception {
        TaskProperties taskProperties = mock(TaskProperties.class);
        TaskContainer taskContainer = mock(TaskContainer.class);
        when(dockerExporterFactory.createExporter(taskProperties)).thenThrow(IllegalArgumentException.class);

        taskExporterSUT.toTaskConfiguration(taskContainer, taskProperties);
    }

    @Test
    public void testToSpecsEntityOK() throws Exception {
        TaskDefinition taskDefinition = mock(TaskDefinition.class);
        TaskDefinitionExporter taskExporterMock = mock(TaskDefinitionExporter.class);
        Task task = mock(Task.class);

        when(taskDefinition.getConfiguration())
                .thenReturn(ImmutableMap.of(DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION, EXPORTER_KEY));
        when(dockerExporterFactory.createExporter(EXPORTER_KEY)).thenReturn(taskExporterMock);
        when(taskExporterMock.toSpecsEntity(taskDefinition)).thenReturn(task);

        Task result = taskExporterSUT.toSpecsEntity(taskDefinition);

        verify(dockerExporterFactory, times(1)).createExporter(EXPORTER_KEY);
        verify(taskExporterMock, times(1)).toSpecsEntity(taskDefinition);
        assertThat(result, sameInstance(task));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testToSpecsEntityWhenCantExportCommandOption() throws Exception {
        TaskDefinition taskDefinition = mock(TaskDefinition.class);

        when(taskDefinition.getConfiguration())
                .thenReturn(ImmutableMap.of(DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION, EXPORTER_KEY));
        when(dockerExporterFactory.createExporter(EXPORTER_KEY)).thenThrow(IllegalArgumentException.class);

        taskExporterSUT.toSpecsEntity(taskDefinition);
    }

    @Test
    public void testValidateOK() throws Exception {
        List<ValidationProblem> validationProblems = Lists.newArrayList(new ValidationProblem("A problem"));
        TaskProperties taskProperties = mock(TaskProperties.class);
        TaskValidationContext taskValidationContext = mock(TaskValidationContext.class);

        TaskDefinitionExporter taskExporterMock = mock(TaskDefinitionExporter.class);
        when(dockerExporterFactory.createExporter(taskProperties)).thenReturn(taskExporterMock);
        when(taskExporterMock.validate(taskValidationContext, taskProperties)).thenReturn(validationProblems);

        List<ValidationProblem> result = taskExporterSUT.validate(taskValidationContext, taskProperties);

        verify(dockerExporterFactory, times(1)).createExporter(taskProperties);
        verify(taskExporterMock, times(1)).validate(taskValidationContext, taskProperties);
        assertThat(result, equalTo(validationProblems));
    }

    @Test
    public void testValidateWhenCantExportProperties() throws Exception {
        TaskProperties taskProperties = mock(TaskProperties.class);
        TaskValidationContext taskValidationContext = mock(TaskValidationContext.class);

        when(dockerExporterFactory.createExporter(taskProperties)).thenThrow(IllegalArgumentException.class);

        List<ValidationProblem> result = taskExporterSUT.validate(taskValidationContext, taskProperties);

        verify(dockerExporterFactory, times(1)).createExporter(taskProperties);
        assertThat(result, equalTo(Collections.emptyList()));
    }

}