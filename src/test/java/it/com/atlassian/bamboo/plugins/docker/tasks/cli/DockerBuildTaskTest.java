package it.com.atlassian.bamboo.plugins.docker.tasks.cli;

import com.atlassian.bamboo.pageobjects.pages.plan.configuration.ArtifactConfigurationPage;
import com.atlassian.bamboo.pageobjects.pages.plan.configuration.JobTaskConfigurationPage;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.testutils.model.TestBuildDetails;
import com.atlassian.bamboo.testutils.model.TestJobDetails;
import com.google.common.collect.ImmutableMap;
import it.com.atlassian.bamboo.plugins.docker.pageobjects.cli.DockerBuildTaskComponent;
import it.com.atlassian.bamboo.plugins.docker.tasks.AbstractDockerTaskTest;
import org.junit.Test;

import java.util.Map;

public class DockerBuildTaskTest extends AbstractDockerTaskTest
{
    @Test
    public void testTask() throws Exception
    {
        final TestBuildDetails plan = createAndSetupPlan();
        final TestJobDetails defaultJob = plan.getDefaultJob();
        final JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, defaultJob);

        // add Docker task
        final Map<String, String> dockerTaskConfig = ImmutableMap.<String, String>builder()
                .put(DockerBuildTaskComponent.REPOSITORY, "myimage")
                .put(DockerBuildTaskComponent.DOCKERFILE_OPTION, DockerBuildTaskComponent.DOCKERFILE_OPTION_INLINE)
                .put(DockerBuildTaskComponent.DOCKERFILE, "FROM scratchimg")
                .put(DockerBuildTaskComponent.SAVE, "true")
                .put(DockerBuildTaskComponent.FILENAME, "myimage.tar")
                .put(DockerBuildTaskComponent.BUILD_OPTIONS, "--label=myimage")
                .put(TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY, "test")
                .build();
        taskConfigurationPage.addNewTask(DockerBuildTaskComponent.TASK_NAME, DockerBuildTaskComponent.class, "Docker build test task", dockerTaskConfig);

        // add artifact definition
        final ArtifactConfigurationPage artifactConfigurationPage = product.visit(ArtifactConfigurationPage.class, defaultJob.getKey());
        artifactConfigurationPage.createArtifactDefinition("Docker image", "test", "*.tar");

        product.gotoHomePage();
        backdoor.plans().triggerBuildAndAwaitSuccess(plan.getKey());
    }
}