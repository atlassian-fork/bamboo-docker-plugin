package com.atlassian.bamboo.plugins.docker;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.task.TaskException;
import org.jetbrains.annotations.NotNull;

public interface PollingService
{
    void waitUntilAvailable(@NotNull final String url, final long timeoutSeconds, @NotNull final BuildLogger logger) throws TaskException;
}
