package com.atlassian.bamboo.plugins.docker.client;

import com.atlassian.bamboo.docker.DockerContainerService;
import com.atlassian.bamboo.docker.DockerException;
import com.atlassian.bamboo.docker.RunConfig;
import com.atlassian.bamboo.plugins.docker.process.DockerProcessService;
import com.atlassian.bamboo.plugins.docker.process.ProcessCommand;
import com.atlassian.bamboo.process.CommandlineStringUtils;
import com.atlassian.bamboo.process.ProcessContext;
import com.atlassian.utils.process.ProcessException;
import com.google.common.base.Joiner;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

public class DockerCmd implements Docker {
    private static final Logger log = Logger.getLogger(DockerCmd.class);

    private static final File USER_HOME = new File(System.getProperty("user.home"));
    private static final File DOCKERCFG = new File(USER_HOME, ".dockercfg");
    private static final File DOCKERCFG_BACKUP = new File(USER_HOME, ".dockercfg.bamboo");

    private final String dockerPath;
    private final DockerProcessService processService;
    private final DockerContainerService dockerContainerService;

    private final ProcessContext processContext;

    public DockerCmd(@NotNull ProcessContext processContext,
                     @NotNull final String dockerPath,
                     @NotNull final DockerProcessService processService,
                     @NotNull final DockerContainerService dockerContainerService) {
        this.dockerPath = dockerPath;
        this.processService = processService;
        this.dockerContainerService = dockerContainerService;
        this.processContext = processContext;
    }

    @Override
    public void run(@NotNull final String imageName, @NotNull final RunConfig runConfig) throws DockerException {
        dockerContainerService.run(processContext,
                Paths.get(dockerPath),
                imageName,
                runConfig,
                processService.getWorkingDirectory(),
                processService.getEnvironmentVariables());
    }

    @Override
    public boolean isRunning(@NotNull final String containerName) throws DockerException {
        return dockerContainerService.isRunning(processContext,
                Paths.get(dockerPath),
                containerName,
                processService.getWorkingDirectory(),
                processService.getEnvironmentVariables());

    }

    @Nullable
    @Override
    public Integer getHostPort(@NotNull final String containerName, @NotNull final Integer containerPort) throws DockerException {
        return dockerContainerService.getHostPort(processContext,
                Paths.get(dockerPath),
                containerName,
                containerPort,
                processService.getWorkingDirectory(),
                processService.getEnvironmentVariables());
    }

    @Override
    public void remove(@NotNull final String containerName) throws DockerException {
        dockerContainerService.remove(processContext,
                Paths.get(dockerPath),
                containerName,
                processService.getWorkingDirectory(),
                processService.getEnvironmentVariables());
    }

    @Override
    public void build(@NotNull final File dockerFolder, @NotNull final String repository,
                      @NotNull final BuildConfig buildConfig) throws DockerException {
        final ProcessCommand.Builder buildCommand = ProcessCommand.builder()
                .add(dockerPath, "build");

        if (buildConfig.isNoCache()) {
            buildCommand.add("--no-cache=true");
        }

        if (!buildConfig.getAdditionalArguments().contains("--force-rm")) {
            buildCommand.add("--force-rm=true");
        }

        if (StringUtils.isNotBlank(buildConfig.getAdditionalArguments())) {
            buildCommand.addAll(CommandlineStringUtils.tokeniseCommandline(buildConfig.getAdditionalArguments()));
        }

        if (!buildConfig.getAdditionalArguments().contains("--tag ")
                && !buildConfig.getAdditionalArguments().contains("--tag=")) {
            buildCommand.add("--tag=" + repository);
        }

        buildCommand.add(dockerFolder.getAbsolutePath());

        try {
            processService.execute(buildCommand.build());
        } catch (ProcessException e) {
            throw new DockerException("Error running Docker build command", e);
        }
    }

    @Override
    public void save(@NotNull final String filename, @NotNull final String repository) throws DockerException {
        final ProcessCommand saveCommand = ProcessCommand.builder()
                .add(dockerPath, "save", "--output=" + filename + "", repository)
                .build();

        try {
            processService.execute(saveCommand);
        } catch (ProcessException e) {
            throw new DockerException("Error running Docker save command", e);
        }
    }

    @Override
    public void tag(@NotNull final String image, @NotNull final String repository, @NotNull final String tag) throws DockerException {
        final ProcessCommand tagCommand = ProcessCommand.builder()
                .add(dockerPath, "tag", image, Joiner.on(':').join(repository, tag))
                .build();

        try {
            processService.execute(tagCommand);
        } catch (ProcessException e) {
            throw new DockerException("Error running Docker tag command", e);
        }
    }

    @Override
    public void push(@NotNull final String repository, @NotNull final AuthConfig authConfig) throws DockerException {
        final Command pushCommand = () -> processService.execute(ProcessCommand.builder()
                .add(dockerPath, "push", repository)
                .build());
        final Command command = authConfig.isAuthorisationProvided()
                ? new AuthorisedCommand(authConfig, pushCommand) : pushCommand;

        try {
            command.execute();
        } catch (ProcessException e) {
            final StringBuilder message = new StringBuilder();
            // The error reason (if one is given) will always occur above this message in the log,
            // i.e. the process stderr is logged before the ProcessException is thrown.
            message.append("Error running Docker push command. See messages above for details.");

            if (!authConfig.isAuthorisationProvided()) {
                message.append(" If no reason is specified it may be because you have not provided")
                        .append(" login details to a registry that requires authorisation.");
            }

            throw new DockerException(message.toString(), e);
        }
    }

    @Override
    public void pull(@NotNull final String repository, @NotNull final AuthConfig authConfig) throws DockerException {
        final Command pullCommand = () -> processService.execute(ProcessCommand.builder()
                .add(dockerPath, "pull", repository)
                .build());
        final Command command = authConfig.isAuthorisationProvided()
                ? new AuthorisedCommand(authConfig, pullCommand) : pullCommand;

        try {
            command.execute();
        } catch (ProcessException e) {
            final StringBuilder message = new StringBuilder();
            // The error reason (if one is given) will always occur above this message in the log,
            // i.e. the process stderr is logged before the ProcessException is thrown.
            message.append("Error running Docker pull command. See messages above for details.");

            if (!authConfig.isAuthorisationProvided()) {
                message.append(" If no reason is specified it may be because you have not provided")
                        .append(" login details to a registry that requires authorisation.");
            }

            throw new DockerException(message.toString(), e);
        }
    }

    private interface Command {
        void execute() throws ProcessException;
    }

    private class AuthorisedCommand implements Command {
        private final AuthConfig authConfig;
        private final Command command;

        private AuthorisedCommand(@NotNull final AuthConfig authConfig, @NotNull final Command command) {
            this.authConfig = authConfig;
            this.command = command;
        }

        @Override
        public void execute() throws ProcessException {
            try {
                backupDockercfg();
                login();
                command.execute();
            } finally {
                logout();
                restoreDockercfg();
            }
        }

        private void backupDockercfg() {
            if (DOCKERCFG.exists()) {
                try {
                    FileUtils.copyFile(DOCKERCFG, DOCKERCFG_BACKUP, true);
                } catch (IOException e) {
                    log.warn("Unable to backup '.dockercfg' file. After the build, make sure the agent's '~/.dockercfg' file is still valid. ", e);
                }
            }
        }

        private void login() throws ProcessException {
            final ProcessCommand.Builder loginCommand = ProcessCommand.builder()
                    .add(dockerPath, "login", "-u", authConfig.getUsername().get(), "--password-stdin")
                    .input(authConfig.getPassword().get());
            if (!authConfig.getEmail().isEmpty()) {
                loginCommand.add("-e", authConfig.getEmail().get());
            }
            if (!authConfig.getRegistryAddress().isEmpty()) {
                loginCommand.add(authConfig.getRegistryAddress().get());
            }
            // Execute silently so that we don't log the password
            processService.executeSilently(loginCommand.build());
        }

        private void logout() throws ProcessException {
            final ProcessCommand.Builder logoutCommand = ProcessCommand.builder().add(dockerPath, "logout");
            if (!authConfig.getRegistryAddress().isEmpty()) {
                logoutCommand.add(authConfig.getRegistryAddress().get());
            }
            processService.execute(logoutCommand.build());
        }

        private void restoreDockercfg() {
            if (DOCKERCFG_BACKUP.exists()) {
                try {
                    DOCKERCFG.delete();
                    FileUtils.moveFile(DOCKERCFG_BACKUP, DOCKERCFG);
                } catch (IOException e) {
                    log.warn("Unable to restore '.dockercfg' file. After the build, make sure the agent's '~/.dockercfg' file is still valid.", e);
                }
            }
        }
    }
}
