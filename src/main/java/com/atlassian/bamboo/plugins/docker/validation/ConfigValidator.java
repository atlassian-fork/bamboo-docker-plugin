package com.atlassian.bamboo.plugins.docker.validation;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import org.jetbrains.annotations.NotNull;

public interface ConfigValidator
{
    void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection);
}
