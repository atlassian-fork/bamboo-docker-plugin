package com.atlassian.bamboo.plugins.docker.utils;

import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.utils.Pair;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class DockerEnvironmentVariablesHelper
{
    private DockerEnvironmentVariablesHelper()
    { }

    private static final Pattern EMPTY_VAR = Pattern.compile("(^|\\s)[^\\s=]+($|\\s)");

    @NotNull
    public static Pair<Map<String, String>, List<String>> splitEnvironmentVariables(
            @NotNull EnvironmentVariableAccessor environmentVariableAccessor,
            @Nullable final String extraEnvironmentVariablesString)
    {
        Map<String, String> extraEnvironmentVariables;
        List<String> passedEnvironment = new ArrayList<>();

        if(extraEnvironmentVariablesString != null)
        {
            extraEnvironmentVariables =
                    environmentVariableAccessor.splitEnvironmentAssignments(extraEnvironmentVariablesString, false);

            Matcher emptyVarMatcher = EMPTY_VAR.matcher(extraEnvironmentVariablesString);
            int startPosition = 0;

            while(emptyVarMatcher.find(startPosition))
            {
                String varName = extraEnvironmentVariablesString.substring(emptyVarMatcher.start() + (emptyVarMatcher.start()!=0 ? 1 : 0),
                        emptyVarMatcher.end() - (emptyVarMatcher.end()!=extraEnvironmentVariablesString.length() ? 1 : 0));
                if(extraEnvironmentVariables.containsKey(varName) && extraEnvironmentVariables.remove(varName, StringUtils.EMPTY))
                {
                    passedEnvironment.add(varName);
                }
                startPosition = emptyVarMatcher.end() - (emptyVarMatcher.end()!=extraEnvironmentVariablesString.length() ? 1 : 0);
            }
        }
        else
        {
            extraEnvironmentVariables = new HashMap<>();
        }

        return Pair.make(extraEnvironmentVariables, passedEnvironment);
    }
}
