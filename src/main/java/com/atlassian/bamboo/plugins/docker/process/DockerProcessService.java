package com.atlassian.bamboo.plugins.docker.process;

import com.atlassian.utils.process.ProcessException;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Path;
import java.util.Map;

public interface DockerProcessService
{
    /**
     * Executes the given command as an external process.
     *
     * @param command The command to execute.
     *
     * @return The output of the external process.
     */
    @NotNull
    String execute(@NotNull final ProcessCommand command) throws ProcessException;

    /**
     * Executes the given command as an external process without logging.
     *
     * @param command The command to execute.
     *
     * @return The output of the external process.
     */
    @NotNull
    String executeSilently(@NotNull final ProcessCommand command) throws ProcessException;


    Path getWorkingDirectory();

    Map<String, String> getEnvironmentVariables();
}
