package com.atlassian.bamboo.plugins.docker.export;

import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.api.model.plan.JobProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.DockerRunContainerTask;
import com.atlassian.bamboo.specs.model.task.docker.AbstractDockerTaskProperties;
import com.atlassian.bamboo.specs.model.task.docker.DockerRunContainerTaskProperties;
import com.atlassian.bamboo.task.TaskContainer;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.export.TaskDefinitionExporter;
import com.atlassian.bamboo.task.export.TaskValidationContext;
import com.atlassian.bamboo.util.Narrow;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.ADDITIONAL_ARGS;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.COMMAND;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.CONTAINER_DATA_VOLUME_PREFIX;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.CONTAINER_PORT_PREFIX;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DEFAULT_CONTAINER_SERVICE_URL;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DEFAULT_CONTAINER_WORK_DIR;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DEFAULT_TIMEOUT_SECONDS;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DETACH;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_RUN;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.ENV_VARS;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.HOST_DIRECTORY_PREFIX;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.HOST_PORT_PREFIX;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.IMAGE;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.LINK;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.NAME;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.SERVICE_TIMEOUT;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.SERVICE_URL_PATTERN;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.SERVICE_WAIT;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.WORK_DIR;
import static com.atlassian.bamboo.specs.model.task.docker.DockerRunContainerTaskProperties.VALIDATION_CONTEXT;
import static com.atlassian.bamboo.task.TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES;
import static com.atlassian.bamboo.task.TaskConfigConstants.CFG_WORKING_SUBDIRECTORY;

@Component
public class RunContainerTaskExporter implements TaskDefinitionExporter {

    private static final Logger log = Logger.getLogger(DockerTaskExporter.class);

    @NotNull
    @Override
    public Map<String, String> toTaskConfiguration(@NotNull TaskContainer taskContainer, @NotNull TaskProperties taskProperties) {
        DockerRunContainerTaskProperties properties = Narrow.downTo(taskProperties, DockerRunContainerTaskProperties.class);
        Preconditions.checkArgument(properties != null, "Couldn't import: " + taskProperties.getClass().getCanonicalName());

        ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();

        builder.put(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_RUN);

        builder.put(IMAGE, properties.getImageName());
        builder.put(DETACH, String.valueOf(properties.isDetachedContainer()));
        builder.put(NAME, StringUtils.defaultString(properties.getContainerName()));

        builder.putAll(portMappingsToConfiguration(properties.getPortMappings()));

        builder.put(SERVICE_WAIT, String.valueOf(properties.isWaitToStart()));
        builder.put(SERVICE_URL_PATTERN, StringUtils.defaultString(properties.getServiceURLPattern()));
        builder.put(SERVICE_TIMEOUT, String.valueOf(properties.getServiceTimeout()));

        builder.put(LINK, String.valueOf(properties.isLinkToDetachedContainers()));
        builder.put(ENV_VARS, StringUtils.defaultString(properties.getContainerEnvironmentVariables()));

        builder.put(COMMAND, StringUtils.defaultString(properties.getContainerCommand()));
        builder.put(WORK_DIR, StringUtils.defaultString(properties.getContainerWorkingDirectory(), DEFAULT_CONTAINER_WORK_DIR));
        builder.put(ADDITIONAL_ARGS, StringUtils.defaultString(properties.getAdditionalArguments()));

        builder.putAll(volumeMappingsToConfiguration(properties.getVolumeMappings()));

        builder.put(CFG_ENVIRONMENT_VARIABLES, StringUtils.defaultString(properties.getEnvironmentVariables()));
        builder.put(CFG_WORKING_SUBDIRECTORY, StringUtils.defaultString(properties.getWorkingSubdirectory()));
        return builder.build();
    }

    private Map<String, String> volumeMappingsToConfiguration(Map<String, String> volumes) {
        Map<String, String> config = new HashMap<>();

        int counter = 0;

        for (Map.Entry<String, String> volumeMapping : volumes.entrySet()) {
            config.put(HOST_DIRECTORY_PREFIX + counter, volumeMapping.getKey());
            config.put(CONTAINER_DATA_VOLUME_PREFIX + counter, volumeMapping.getValue());
            counter++;
        }
        return config;
    }

    private Map<String, String> portMappingsToConfiguration(Map<Integer, Integer> portMappings) {
        Map<String, String> portMappingConfig = new HashMap<>();
        int counter = 0;
        for (Map.Entry<Integer, Integer> portMapping : portMappings.entrySet()) {
            portMappingConfig.put(HOST_PORT_PREFIX + counter, String.valueOf(portMapping.getKey()));
            portMappingConfig.put(CONTAINER_PORT_PREFIX + counter, String.valueOf(portMapping.getValue()));
            counter++;
        }
        return portMappingConfig;
    }

    @NotNull
    @Override
    public Task toSpecsEntity(@NotNull TaskDefinition taskDefinition) {
        if (isDockerRunContainerTask(taskDefinition)) {
            Map<String, String> configuration = taskDefinition.getConfiguration();

            DockerRunContainerTask task = new DockerRunContainerTask();

            task.imageName(configuration.getOrDefault(IMAGE, ""));

            task.detachContainer(Boolean.parseBoolean(configuration.getOrDefault(DETACH, "false")));
            task.containerName(configuration.getOrDefault(NAME, ""));

            task.clearPortMappings();
            for (Map.Entry<Integer, Integer> portMapping : configMapToPortMappings(taskDefinition.getConfiguration()).entrySet()) {
                task.appendPortMapping(portMapping.getKey(), portMapping.getValue());
            }

            task.waitToStart(Boolean.parseBoolean(configuration.getOrDefault(SERVICE_WAIT, "false")));
            task.serviceURLPattern(configuration.getOrDefault(SERVICE_URL_PATTERN, DEFAULT_CONTAINER_SERVICE_URL));

            long timeout = DEFAULT_TIMEOUT_SECONDS;
            try {
                if (StringUtils.isNotBlank(configuration.get(SERVICE_TIMEOUT))) {
                    timeout = Long.parseLong(configuration.get(SERVICE_TIMEOUT));
                }
            } catch (NumberFormatException ex) {
                log.warn(String.format("Task by id: %d has invalid timeout configuration: '%s'. Assuming default value.",
                        taskDefinition.getId(),
                        configuration.get(SERVICE_TIMEOUT)));
                log.debug(ex);
            } finally {
                task.serviceTimeoutInSeconds(timeout);
            }

            task.linkToDetachedContainers(Boolean.parseBoolean(configuration.getOrDefault(LINK, "false")));
            task.containerEnvironmentVariables(configuration.getOrDefault(ENV_VARS, ""));
            task.containerCommand(configuration.getOrDefault(COMMAND, ""));
            task.containerWorkingDirectory(configuration.getOrDefault(WORK_DIR, DEFAULT_CONTAINER_WORK_DIR));
            task.additionalArguments(configuration.getOrDefault(ADDITIONAL_ARGS, ""));

            task.clearVolumeMappings();
            for (Map.Entry<String, String> volumeMapping : configMapToVolumeMappings(taskDefinition.getConfiguration()).entrySet()) {
                task.appendVolumeMapping(volumeMapping.getKey(), volumeMapping.getValue());
            }

            task.environmentVariables(configuration.getOrDefault(CFG_ENVIRONMENT_VARIABLES, ""));
            task.workingSubdirectory(configuration.getOrDefault(CFG_WORKING_SUBDIRECTORY, ""));

            return task;
        }
        throw new IllegalArgumentException(String.format("Couldn't export task by id: %s and plugin key: %s", taskDefinition.getId(), taskDefinition.getPluginKey()));
    }

    private Map<Integer, Integer> configMapToPortMappings(Map<String, String> configuration) {
        ImmutableMap.Builder<Integer, Integer> portMappings = ImmutableMap.builder();

        for (Map.Entry<String, String> entry : configuration.entrySet()) {
            if (entry.getKey().contains(HOST_PORT_PREFIX)) {
                try {
                    String counter = entry.getKey().substring(HOST_PORT_PREFIX.length());
                    int hostPort = Integer.parseInt(entry.getValue());
                    int containerPort = Integer.parseInt(configuration.get(CONTAINER_PORT_PREFIX + counter));
                    portMappings.put(hostPort, containerPort);
                } catch (NumberFormatException ex) {
                    log.warn("Docker Run Container task export. Invalid port mapping in configuration map. Ignoring entry", ex);
                }
            }
        }
        return portMappings.build();
    }

    private Map<String, String> configMapToVolumeMappings(Map<String, String> configuration) {
        ImmutableMap.Builder<String, String> volumeMappings = ImmutableMap.builder();

        for (Map.Entry<String, String> entry : configuration.entrySet()) {
            if (entry.getKey().contains(HOST_DIRECTORY_PREFIX)) {
                String counter = entry.getKey().substring(HOST_DIRECTORY_PREFIX.length());
                String hostVolume = entry.getValue();
                String containerVolume = configuration.get(CONTAINER_DATA_VOLUME_PREFIX + counter);

                volumeMappings.put(hostVolume, containerVolume);
            }
        }
        return volumeMappings.build();
    }

    private boolean isDockerRunContainerTask(@NotNull TaskDefinition taskDefinition) {
        return taskDefinition.getPluginKey().equals(AbstractDockerTaskProperties.MODULE_KEY.getCompleteModuleKey())
                && DOCKER_COMMAND_OPTION_RUN.equals(taskDefinition.getConfiguration().get(DOCKER_COMMAND_OPTION));
    }

    @Override
    public List<ValidationProblem> validate(@NotNull TaskValidationContext taskValidationContext, @NotNull TaskProperties taskProperties) {
        Optional<JobProperties> job = taskValidationContext.getOwnerJob();
        if (job.isPresent()) {
            DockerRunContainerTaskProperties properties = Narrow.downTo(taskProperties, DockerRunContainerTaskProperties.class);

            if (properties != null && properties.isDetachedContainer()) {
                Set<String> detachedContainersNames = job.get().getTasks().stream()
                        .map(task -> Narrow.downTo(task, DockerRunContainerTaskProperties.class))
                        .filter(Objects::nonNull)
                        .filter(task -> task != properties)
                        .filter(DockerRunContainerTaskProperties::isDetachedContainer)
                        .map(DockerRunContainerTaskProperties::getContainerName)
                        .collect(Collectors.toSet());

                //properties.getContainerName() shouldn't be null on detached containers. Validation performed in
                // com.atlassian.bamboo.specs.model.task.docker.DockerRunContainerTaskProperties.validate()

                if (detachedContainersNames.contains(properties.getContainerName())) {
                    return Collections.singletonList(new ValidationProblem(VALIDATION_CONTEXT,
                            String.format("Name '%s' is already assigned to a detached container in this job",
                                    properties.getContainerName())));

                }
            }
        } else {
            log.info("Can't validate containers name since job properties is not present.");
        }
        return Collections.emptyList();
    }
}
