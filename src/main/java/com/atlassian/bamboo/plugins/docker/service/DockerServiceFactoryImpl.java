package com.atlassian.bamboo.plugins.docker.service;

import com.atlassian.bamboo.plugins.docker.PollingService;
import com.atlassian.bamboo.plugins.docker.client.Docker;
import com.atlassian.bamboo.plugins.docker.export.BuildImageTaskExporter;
import com.atlassian.bamboo.plugins.docker.export.RegistryTaskExporter;
import com.atlassian.bamboo.plugins.docker.export.RunContainerTaskExporter;
import com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.model.task.docker.DockerBuildImageTaskProperties;
import com.atlassian.bamboo.specs.model.task.docker.DockerRegistryTaskProperties;
import com.atlassian.bamboo.specs.model.task.docker.DockerRunContainerTaskProperties;
import com.atlassian.bamboo.task.export.TaskDefinitionExporter;
import com.atlassian.bamboo.util.Narrow;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.inject.Inject;


@ExportAsService(DockerServiceFactory.class)
@Component("serviceFactory")
public class DockerServiceFactoryImpl implements DockerServiceFactory
{
    private final EnvironmentVariableAccessor environmentVariableAccessor;
    private final PollingService pollingService;

    @Inject
    public DockerServiceFactoryImpl(@ComponentImport final EnvironmentVariableAccessor environmentVariableAccessor,
                                    final PollingService pollingService) {
        this.environmentVariableAccessor = environmentVariableAccessor;
        this.pollingService = pollingService;
    }

    @NotNull
    @Override
    public DockerService create(@NotNull final Docker docker, @NotNull final String dockerCommandOption) {
        if (DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_BUILD.equals(dockerCommandOption)) {
            return new BuildService(docker);
        }

        if (DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_RUN.equals(dockerCommandOption)) {
            return new RunService(docker, environmentVariableAccessor, pollingService);
        }

        if (DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_PUSH.equals(dockerCommandOption)) {
            return new PushService(docker);
        }

        if (DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_PULL.equals(dockerCommandOption)) {
            return new PullService(docker);
        }

        throw new IllegalArgumentException("No service for option " + dockerCommandOption);
    }
}
