package com.atlassian.bamboo.plugins.docker.validation;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plugins.docker.RepositoryKey;
import com.atlassian.bamboo.plugins.docker.RepositoryKeys;
import com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.sal.api.message.I18nResolver;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

public class BuildConfigValidator implements ConfigValidator
{
    private I18nResolver i18nResolver;

    public BuildConfigValidator(I18nResolver i18nResolver)
    {
        this.i18nResolver = i18nResolver;
    }

    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection)
    {
        final RepositoryKey repositoryKey = RepositoryKeys.parseKey(StringUtils.defaultString(params.getString(DockerCliTaskConfigurator.REPOSITORY)));

        if (StringUtils.isBlank(repositoryKey.getRepository()))
        {
            errorCollection.addError(DockerCliTaskConfigurator.REPOSITORY, i18nResolver.getText("docker.repository.error.empty"));
        }

        if (DockerCliTaskConfigurator.DOCKERFILE_OPTION_INLINE.equals(params.getString(DockerCliTaskConfigurator.DOCKERFILE_OPTION)))
        {
            if (StringUtils.isBlank(params.getString(DockerCliTaskConfigurator.DOCKERFILE)))
            {
                errorCollection.addError(DockerCliTaskConfigurator.DOCKERFILE, i18nResolver.getText("docker.dockerfile.error.empty"));
            }
        }

        if (params.getBoolean(DockerCliTaskConfigurator.SAVE))
        {
            if (StringUtils.isBlank(params.getString(DockerCliTaskConfigurator.FILENAME)))
            {
                errorCollection.addError(DockerCliTaskConfigurator.FILENAME, i18nResolver.getText("docker.save.filename.error.empty"));
            }
        }
    }
}